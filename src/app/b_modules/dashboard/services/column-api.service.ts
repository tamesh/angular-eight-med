import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import data from 'assets/data/medline-data.json';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ColumnApiService {

  constructor(private http: HttpClient) {
    // console.log(data);
  }

  getColumnDetails() {
    // return this.http.get('../assets/data/medline-data.json');
    return of(data);
  }
}
