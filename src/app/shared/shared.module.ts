import { NgModule } from '@angular/core';
import { AngularModule } from './angular/angular.module';
import { PrimengModule } from './primeng/primeng.module';
import { PageNoFoundComponent } from './components/page-no-found/page-no-found.component';
import { MainHeaderComponent } from './components/main-header/main-header.component';
import { MainHeaderTabComponent } from './components/main-header-tab/main-header-tab.component';
import { CommonMenuTabsComponent } from './components/common-menu-tabs/common-menu-tabs.component';
import { RefreshTabComponent } from './components/refresh-tab/refresh-tab.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { YesNoPipe } from './pipes/trueFalseToYesNo.pipe';
import { NgxIndexDbModule } from './ngx-index-db/ngx-index-db.module';

const COMMON_COMPONENTS = [
  MainHeaderComponent,
  MainHeaderTabComponent,
  CommonMenuTabsComponent,
  RefreshTabComponent,
  SpinnerComponent
];

const COMMON_PIPES = [
  YesNoPipe
];

@NgModule({
  declarations: [
    ...COMMON_COMPONENTS,
    PageNoFoundComponent,
    ...COMMON_PIPES
  ],
  imports: [
    AngularModule, PrimengModule, NgxIndexDbModule
  ],
  exports: [
    AngularModule, PrimengModule, NgxIndexDbModule, ...COMMON_COMPONENTS, ...COMMON_PIPES
  ],
})
export class SharedModule { }
