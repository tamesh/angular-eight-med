import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class ApplicationDataService {
    private env: any;

    constructor(
        private http: HttpClient,
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        return this.initEnvironments();
    }

    initEnvironments(): any {
        if (this.env === undefined || this.env === 'undefined' || this.env === null) {
            return this.http.get('/assets/env/environment.json')
                .pipe(
                    map((res: any) => {
                        this.env = res;
                        return this.env;
                    })
                );
        } else {
            // return Observable.empty<Response>();
        }
    }
}
