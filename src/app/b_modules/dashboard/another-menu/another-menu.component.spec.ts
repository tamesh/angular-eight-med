import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnotherMenuComponent } from './another-menu.component';

describe('AnotherMenuComponent', () => {
  let component: AnotherMenuComponent;
  let fixture: ComponentFixture<AnotherMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnotherMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnotherMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
