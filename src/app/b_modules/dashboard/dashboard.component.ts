import { Component, OnInit } from '@angular/core';

export interface TabLists {
  label: string;
  tabId: string;
  tabIndex: number;
  routerLink?: Array<string>;
  closeable: boolean;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public tabList: TabLists[] = [
    { label: 'Menu One', tabId: 'DASHBOARD', tabIndex: 0, routerLink: ['dashboard-details'], closeable: false },
    { label: 'Another Menu', tabId: 'ANO_MENU', tabIndex: 1, routerLink: ['another-menu'], closeable: false }
  ];

  constructor() { }

  ngOnInit() {
  }

}
