import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthService {
  private token: any;
  private loginName: any;
  private userRole: any;

  constructor(
    private http: HttpClient,
    private router: Router,
  ) {
  }

  isUserSessionAvailable() {
    return true;
  }

  hasRoutePermission(url) {
    return true;
  }
}
