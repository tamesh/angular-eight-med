import { NgModule } from '@angular/core';

import { NgxIndexedDBModule } from 'ngx-indexed-db';

export interface StoreConfig {
  keyPath: string;
  autoIncrement: boolean;
}

export interface StoreSchema {
  name: string;
  keypath: string;
  options: { unique: boolean };
}

export interface StoreMedataData {
  store: string;
  storeConfig: StoreConfig;
  storeSchema: StoreSchema[];
}

export interface DBConfig {
  name: string;
  version: number;
  objectStoresMeta: StoreMedataData[];
}

const dbConfig: DBConfig = {
  name: 'Medline', version: 1, objectStoresMeta: [
    {
      store: 'columnTable',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'id', keypath: 'id', options: { unique: true } },
        { name: 'colName', keypath: 'colName', options: { unique: false }},
        { name: 'colType', keypath: 'colType', options: { unique: false }},
        { name: 'colEditable', keypath: 'colEditable', options: { unique: false }},
      ]
    }
  ]
};


@NgModule({
  declarations: [],
  imports: [
    NgxIndexedDBModule.forRoot(dbConfig)
  ]
})
export class NgxIndexDbModule { }
