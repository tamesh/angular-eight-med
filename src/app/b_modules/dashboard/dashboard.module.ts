import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
// import { TableModule } from 'primeng/table';

import { DashboardComponent } from './dashboard.component';
import { DashboardDetailsComponent } from './dashboard-details/dashboard-details.component';
import { MenuOneComponent } from './menu-one/menu-one.component';
import { AnotherMenuComponent } from './another-menu/another-menu.component';

const MAIN_COMPONENT = [
  DashboardComponent, DashboardDetailsComponent
];

@NgModule({
  declarations: [
    ...MAIN_COMPONENT,
    MenuOneComponent,
    AnotherMenuComponent
  ],
  imports: [
    SharedModule,
    DashboardRoutingModule,
  ],
})
export class DashboardModule { }
