import { Component, OnInit, OnDestroy} from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit, OnDestroy {

  constructor() {
  }

  ngOnInit() {

  }

  ngOnDestroy() {

  }

}
