import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

export interface UserInterface {
  userName: string;
  userPassword: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public user: UserInterface = {
    userName: null,
    userPassword: null,
  };
  public isLoading = false;

  constructor(private route: Router) { }

  ngOnInit() {

  }

  onLogin() {
    console.log('User: ', this.user );
    this.route.navigateByUrl('web');
  }

}
