export interface ColumnData {
    id: string;
    colName: string;
    colType: string;
    colEditable: boolean;
}

export interface DropDownType {
    label: string;
    value: string;
}

export interface DropDownTrueFalse {
    label: string;
    value: boolean;
}

export interface ResData {
    data: ColumnData[];
}

export const generateUID = () => {
    return 'xxxxxxx1xxxx4xxx5yxxx6xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        let r = Math.random() * 16 | 0;
        let v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};

export const columnType: DropDownType[] = [{
    label: 'String',
    value: 'String'
},
{
    label: 'Number',
    value: 'Number'
},
{
    label: 'Date',
    value: 'Date'
},
{
    label: 'Boolean',
    value: 'Boolean'
}];

export const columnEditable: DropDownTrueFalse[] = [{
    label: 'Yes',
    value: true
}, {
    label: 'No',
    value: false
}];

export const defaultModel: ColumnData = {
    id: generateUID(),
    colName: '',
    colType: '',
    colEditable: false
};

// export interface ResultGridInterface {
//     colName: string;
//     colType: string;
//     isEditable: boolean;
// }

// export const resultData: ResultGridInterface[] = [{
//     colName: 'Column',
//     colType: 'Number',
//     isEditable: false
// },
// {
//     colName: 'Column 2',
//     colType: 'String',
//     isEditable: true
// }];



