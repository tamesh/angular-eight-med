import { Injectable, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { ColumnData } from '../constant-data';
import jsonData from 'assets/data/medline-data.json';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {

  private STORAGE_KEY = 'medlineDB';

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
    // this.storage.set(this.STORAGE_KEY, jsonData.data);
  }

  public getStoreOnLocalStorate(): ColumnData[] {
    return this.storage.get(this.STORAGE_KEY) || [];
  }

  public storeOnLocalStorage(item: ColumnData): void {
    const currentData = this.storage.get(this.STORAGE_KEY) || [];
    currentData.push(item);
    this.storage.set(this.STORAGE_KEY, currentData);
  }

  public deleteStoreOnLocalStorage(items: ColumnData[]): void {
    const currentData = this.storage.get(this.STORAGE_KEY);
    let updatedList = null;
    for (const item of items) {
      updatedList = currentData.filter((ele) => ele.id !== item.id);
    }
    this.storage.set(this.STORAGE_KEY, updatedList);
  }

  public updateStoreOnLocalStorage(item: ColumnData): void {
    const currentData = this.storage.get(this.STORAGE_KEY);
    const itemIndx = currentData.findIndex(ele => ele.id === item.id);
    currentData[itemIndx] = item;
    this.storage.set(this.STORAGE_KEY, currentData);
  }
}
