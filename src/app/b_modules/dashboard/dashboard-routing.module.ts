import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { DashboardDetailsComponent } from './dashboard-details/dashboard-details.component';
import { MenuOneComponent } from './menu-one/menu-one.component';
import { AnotherMenuComponent } from './another-menu/another-menu.component';

export const childRoutes: Routes = [
  {
    path: 'dashboard-details',
    component: DashboardDetailsComponent,
  },
  {
    path: 'menu-one',
    component: MenuOneComponent,
  },
  {
    path: 'another-menu',
    component: AnotherMenuComponent,
  }
];

export const DashboardRoutes: Routes = [
  {
    path: '', pathMatch: 'full', redirectTo: 'dashboard-details'
  },
  {
    children: childRoutes,
    component: DashboardComponent,
    path: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(DashboardRoutes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule { }
