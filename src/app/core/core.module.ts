import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  ApplicationDataService,
  AuthService,
} from './services';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    ApplicationDataService,
    AuthService,
  ]
})
export class CoreModule {
}
