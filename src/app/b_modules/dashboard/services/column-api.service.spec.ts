import { TestBed } from '@angular/core/testing';

import { ColumnApiService } from './column-api.service';

describe('ColumnApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ColumnApiService = TestBed.get(ColumnApiService);
    expect(service).toBeTruthy();
  });
});
