import { Component, OnInit } from '@angular/core';
import {
  ColumnData, DropDownType, columnType,
  columnEditable, defaultModel,
  DropDownTrueFalse
} from '../constant-data';

import { ColumnApiService } from '../services/column-api.service';

import { NgxIndexedDBService } from 'ngx-indexed-db';
import { DataStorageService } from '../services/data-storage.service';
import { MessageService } from 'primeng/api';
import { v4 as uuid } from 'uuid';


@Component({
  selector: 'app-dashboard-details',
  templateUrl: './dashboard-details.component.html',
  styleUrls: ['./dashboard-details.component.scss'],
  providers: [DataStorageService, MessageService]
})
export class DashboardDetailsComponent implements OnInit {

  public columnData: ColumnData[];
  public colRecord: ColumnData = defaultModel;
  public colType: DropDownType[] = columnType;
  public colEditable: DropDownTrueFalse[] = columnEditable;
  public selectedRow: ColumnData[];

  public resultRowSelection: ColumnData[];
  public resultGrid: ColumnData[];

  public columnReadbleData: ColumnData[];

  public display = false;

  constructor(
    private columnApi: ColumnApiService,
    private dbService: DataStorageService,
    private msgService: MessageService) {
    // this.columnApi.getColumnDetails().subscribe((res: any) => {
    //   this.columnData = res.data;
    // });
    this.columnData = dbService.getStoreOnLocalStorate();
    this.columnReadbleData = dbService.getStoreOnLocalStorate();
  }

  ngOnInit() {

  }

  openPopUp() {
    defaultModel.id = uuid();
    this.display = true;
    this.colRecord = JSON.parse(JSON.stringify(defaultModel));
  }

  save() {
    const deepCopyItem = JSON.parse(JSON.stringify(this.colRecord));
    this.columnData.push(deepCopyItem);
    const colmRdble = JSON.parse(JSON.stringify(this.colRecord));
    this.columnReadbleData.push(colmRdble);
    this.display = false;
    this.dbService.storeOnLocalStorage(deepCopyItem);
    this.msgService.add({ severity: 'success', summary: 'Row Record', detail: 'Record added successfully' });
  }

  deleteRecord() {
    if (this.resultRowSelection && this.resultRowSelection.length > 0) {
      this.dbService.deleteStoreOnLocalStorage(this.resultRowSelection);
      for (const item of this.resultRowSelection) {
        this.columnData = this.columnData.filter((ele) => ele.id !== item.id);
        this.columnReadbleData = this.columnReadbleData.filter((ele) => ele.id !== item.id);
      }
      this.resultRowSelection = null;
      this.msgService.add({ severity: 'success', summary: 'Record Deleted', detail: 'Record deleted successfully' });
    } else {
      this.msgService.add({ severity: 'error', summary: 'Row Record', detail: 'Row details record not selected' });
    }
  }

  newData(data) {
    const foundCcd = data.filter(ele => ele.id.match('ccd'));
  }

  updateRow(item: ColumnData) {
    const deepCopyItem = JSON.parse(JSON.stringify(item));
    this.dbService.updateStoreOnLocalStorage(deepCopyItem);
    const itemIndx = this.columnData.indexOf(item);
    this.columnReadbleData[itemIndx] = deepCopyItem;
    this.msgService.add({ severity: 'success', summary: 'Record Updated', detail: 'Record updated successfully'});
  }

  deleteSingleRow(item: ColumnData) {
    this.columnData = this.columnData.filter((ele) => ele.id !== item.id);
    this.columnReadbleData = this.columnReadbleData.filter((ele) => ele.id !== item.id);
    this.dbService.deleteStoreOnLocalStorage([item]);
    this.msgService.add({ severity: 'success', summary: 'Record Deleted', detail: 'Record deleted successfully' });
  }

}
